package swim.game

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Canvas
import android.graphics.Paint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import java.util.*

class Game : AppCompatActivity(), SensorEventListener {


        var BALL_SIZE = 70
        var MIN_DISTANCE_PROXIMITY = 2
        var SCORE_CHANGE_MODE = 200
        var BALL_POS_MARGIN = 100
        var TIME_DELAY = 500
        var HS_DEF_VALUE = 0
        var FIRST_RADIUS = 0.1
        var SECOND_RADIUS = 0.25
        var THIRD_RADIUS = 0.4
        var FOURTH_RADIUS = 0.55
        var FIFTH_RADIUS = 0.7
        var SIXTH_RADIUS = 0.85
        var MAX_RADIUS = 20
        var COVER_SCREEN_MULTIPLIER = 2
        var ALPHA_LEVEL = 10
        var CURR_SCORE_TOP_MARGIN = 120F
        var OVERLAY_TEXTSIZE = 150F
        var NO_TRANSPARENT = 255
        var SCORE = 0
        var CENTER_X: Int = 0
        var BALL_X_POS: Int = 0
        var BALL_X_ACC: Int = 0
        var BALL_X_VEL: Int = 0
        var MAX_X = 0
        var CENTER_Y: Int = 0
        var BALL_Y_POS: Int = 0
        var BALL_Y_ACC: Int = 0
        var BALL_Y_VEL: Int = 0
        var MAX_Y = 0
        var SHOW_GAME: Boolean = false
        var SWITCH_CONTROLS: Boolean = false
        var SWITCH_COUNTER = 0
        var currentColor: Int = R.color.baseColor
        var frameTime = 0.3333f


    private lateinit var sensorManager: SensorManager
    private lateinit var gameView: MyView
    private lateinit var random: Random
    private lateinit var runnable : Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        random = Random()
        val h = Handler()
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.KEEP_SCREEN_ON)
        //		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams
        //		.FLAG_FULLSCREEN);
        //		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

//        val display = getWindowManager().getDefaultDisplay()
//        display.getSize(p)

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        MAX_X = displayMetrics.widthPixels
        MAX_Y = displayMetrics.heightPixels
        CENTER_X = displayMetrics.widthPixels / 2
        CENTER_Y = displayMetrics.heightPixels / 2

        BALL_X_POS = random.nextInt(displayMetrics.widthPixels - BALL_POS_MARGIN) + 50
        BALL_Y_POS = random.nextInt(displayMetrics.heightPixels - BALL_POS_MARGIN) + 50

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        gameView = MyView(this)

        setContentView(gameView)

        runnable = Runnable {
            val s = calcPoints()
//            CHECK_SWITCH_CONTROL = SWITCH_CONTROLS
            if (SCORE > SCORE_CHANGE_MODE && SWITCH_COUNTER == 0) {
                SWITCH_CONTROLS = switchChance()
            }

            if (SWITCH_COUNTER > 50) {
                SWITCH_CONTROLS = switchChance()
            }

            if (SWITCH_CONTROLS) {
                SWITCH_COUNTER = 0
                currentColor = R.color.switchColor
            } else if (!SWITCH_CONTROLS) {
                SWITCH_COUNTER = 0
                currentColor = R.color.baseColor
//                baseColor = switchColor
//                switchColor = currentColor
            }
            if (SWITCH_CONTROLS) {
                SWITCH_COUNTER++
            }

            SCORE += s
            h.postDelayed(runnable, TIME_DELAY.toLong())
        }

        h.postDelayed(runnable, TIME_DELAY.toLong())
    }

    override fun onResume() {
        super.onResume()
        val sensorProximity = sensorManager.getSensorList(Sensor.TYPE_PROXIMITY)[0]
        val sensorAcc = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)[0]
        sensorManager.registerListener(this, sensorProximity, SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager.registerListener(this, sensorAcc, SensorManager.SENSOR_DELAY_GAME)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
        val sp : SharedPreferences = getSharedPreferences("game", Context.MODE_PRIVATE)
        val ed = sp.edit()
        val currBest = sp.getInt("highscore", HS_DEF_VALUE)
        ed.putInt("highscore", Math.max(SCORE, currBest))
        ed.apply()
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            BALL_X_ACC = event.values[0].toInt()
            BALL_Y_ACC = event.values[1].toInt()
            updateBall()
        } else if (event.sensor.type == Sensor.TYPE_PROXIMITY) {
            SHOW_GAME = event.values[0] > MIN_DISTANCE_PROXIMITY
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //pass
    }

    private fun changeAccChance(): Boolean {
        val currentPos = calcPoints()
        return random.nextInt(101) > 95 - currentPos - (0.001 * SCORE).toInt()
        //5% chance; in closer to center 6% - 11% chance; additional 1% chance per 1000 score
    }

    private fun changeAccValue(): Int {
        var sign = 0
        when (random.nextInt(2)) {
            0 -> sign = -1
            1 -> sign = 1
        }
        return sign * random.nextInt(Math.min((0.1 * SCORE + 1).toInt(), (MAX_X / 1.5).toInt()))
    }

    private fun switchChance(): Boolean {
        return random.nextInt(101) > 70 //constant 30% chance to switch controls
    }

    private fun updateBall() {
        if (changeAccChance()) {
            BALL_X_ACC += changeAccValue()
            BALL_Y_ACC += changeAccValue()
        }

        BALL_X_VEL += BALL_X_ACC
        BALL_Y_VEL += BALL_Y_ACC

//        val xS = BALL_X_VEL / 2 * frameTime
//        val yS = BALL_Y_VEL / 2 * frameTime
        val xS = BALL_X_VEL / 1.5 * frameTime
        val yS = BALL_Y_VEL / 1.5 * frameTime
        if (SWITCH_CONTROLS) {
            BALL_X_POS += xS.toInt()
            BALL_Y_POS -= yS.toInt()
        } else {
            BALL_X_POS -= xS.toInt()
            BALL_Y_POS += yS.toInt()
        }
        if (BALL_X_POS <= 0 || BALL_X_POS >= MAX_X || BALL_Y_POS <= 0 || BALL_Y_POS >= MAX_Y) {
            finish()
        }
    }

    private fun calcPoints(): Int {
        var points = 0
        if (BALL_X_POS < CENTER_X + FIRST_RADIUS*MAX_X && BALL_X_POS > CENTER_X - FIRST_RADIUS*MAX_X && BALL_Y_POS < CENTER_Y + FIRST_RADIUS*MAX_X && BALL_Y_POS > CENTER_Y - FIRST_RADIUS*MAX_X) {
            points = MAX_RADIUS
        } else if (BALL_X_POS < CENTER_X + SECOND_RADIUS*MAX_X && BALL_X_POS > CENTER_X - SECOND_RADIUS*MAX_X && BALL_Y_POS < CENTER_Y + SECOND_RADIUS*MAX_X && BALL_Y_POS > CENTER_Y - SECOND_RADIUS*MAX_X) {
            points = MAX_RADIUS - 5
        } else if (BALL_X_POS < CENTER_X + THIRD_RADIUS*MAX_X && BALL_X_POS > CENTER_X - THIRD_RADIUS*MAX_X && BALL_Y_POS < CENTER_Y + THIRD_RADIUS*MAX_X && BALL_Y_POS > CENTER_Y - THIRD_RADIUS*MAX_X) {
            points = MAX_RADIUS - 8
        } else if (BALL_X_POS < CENTER_X + FOURTH_RADIUS*MAX_X && BALL_X_POS > CENTER_X - FOURTH_RADIUS*MAX_X && BALL_Y_POS < CENTER_Y + FOURTH_RADIUS*MAX_X && BALL_Y_POS > CENTER_Y - FOURTH_RADIUS*MAX_X) {
            points = MAX_RADIUS - 10
        } else if (BALL_X_POS < CENTER_X + FIFTH_RADIUS*MAX_X && BALL_X_POS > CENTER_X - FIFTH_RADIUS*MAX_X && BALL_Y_POS < CENTER_Y + FIFTH_RADIUS*MAX_X && BALL_Y_POS > CENTER_Y - FIFTH_RADIUS*MAX_X) {
            points = MAX_RADIUS - 12
        } else if (BALL_X_POS < CENTER_X + SIXTH_RADIUS*MAX_X && BALL_X_POS > CENTER_X - SIXTH_RADIUS*MAX_X && BALL_Y_POS < CENTER_Y + SIXTH_RADIUS*MAX_X && BALL_Y_POS > CENTER_Y - SIXTH_RADIUS*MAX_X) {
            points = MAX_RADIUS - 13
        }
        if (!SHOW_GAME) points *= COVER_SCREEN_MULTIPLIER
        return points
    }

    open inner class MyView(context: Context) : View(context) {
        private var paint: Paint = Paint()
        private var x: Int = 0
        private var y: Int = 0

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            x = width
            y = height
            paint.textSize = 100F
            paint.typeface = ResourcesCompat.getFont(context, R.font.poiret_one)
            if (SHOW_GAME) {
                drawBackground(canvas)
                paint.style = Paint.Style.FILL
                paint.color = resources.getColor(currentColor)
                canvas.drawCircle(BALL_X_POS.toFloat(), BALL_Y_POS.toFloat(), BALL_SIZE.toFloat(), paint)
            } else {
                drawOverlay(canvas)
            }
            invalidate()
        }

        private fun drawBackground(canvas: Canvas) {
            paint.style = Paint.Style.FILL
            paint.color = resources.getColor(R.color.light_overlay)
            canvas.drawPaint(paint)
            paint.color = resources.getColor(currentColor)
            paint.alpha = ALPHA_LEVEL
            for (i in 0..5) {
                canvas.drawCircle((x / 2).toFloat(), (y / 2).toFloat(), (BALL_SIZE + 50 + i * 100).toFloat(), paint)
            }
            paint.alpha = NO_TRANSPARENT
            paint.textAlign = Paint.Align.CENTER
            canvas.drawText("score: $SCORE", (x / 2).toFloat(), CURR_SCORE_TOP_MARGIN, paint)
            //			canvas.drawText("X:" + BALL_X_POS + ", Y:" + BALL_Y_POS, x / 2, y / 2, paint);
        }

        private fun drawOverlay(canvas: Canvas) {
            paint.alpha = NO_TRANSPARENT
            paint.color = resources.getColor(R.color.dark_overlay)
            canvas.drawPaint(paint)
            paint.alpha = NO_TRANSPARENT
            paint.color = resources.getColor(currentColor)
            paint.textAlign = Paint.Align.CENTER
            paint.textSize = OVERLAY_TEXTSIZE
            canvas.drawText("score: $SCORE", (x / 2).toFloat(), (y / 2).toFloat(), paint)
        }
    }
}